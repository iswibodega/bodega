<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHasPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        $tableNames = config('permission.table_names');
        $columnNames = config('permission.column_names');
        
       /*   Schema::create($tableNames['user_has_permissions'], function (Blueprint $table) use ($tableNames) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('permission_id');

             $table->foreign('user_id')
                ->references('id')
                ->on($tableNames['users'])
                ->onDelete('cascade');

            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');           

            $table->primary([ 'user_id','permission_id'], 'user_has_permissions_permission_id_users_id_primary');
        });*/


        Schema::create($tableNames['user_has_roles'], function (Blueprint $table) use ($tableNames, $columnNames) {
            $table->unsignedInteger('role_id');
            $table->unsignedInteger('user_id');

             $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on($tableNames['users'])
                ->onDelete('cascade');


            $table->primary(['role_id', 'user_id'],
                    'user_has_roles_role_users_id_primary');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         $tableNames = config('permission.table_names');

        Schema::drop($tableNames['user_has_permissions']);
        Schema::drop($tableNames['user_has_roles']);           
        
    }
}
