<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class EntradaController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index($value='')
    {
    	# code...
    	$producto_list=DB::select("SELECT A.FECHA_INVENTARIO
                                          ,CASE WHEN A.MOVIMIENTO='1' THEN 'ENTRADA'
                                                WHEN A.MOVIMIENTO='2' THEN 'SALIDA'
                                                ELSE A.MOVIMIENTO
                                            END MOVIMIENTO
                                          ,A.ID_BODEGA
                                          ,A.UNIDAD
                                          ,A.CANTIDAD
                                          ,A.DOCUMENTO
                                          ,A.RESPONSABLE
                                          ,B.NOMBRE_PRODUCTO 
                                          FROM INVENTARIO A INNER JOIN PRODUCTO B ON A.ID_PRODUCTO=B.ID_PRODUCTO");

			return view('entrada',compact('producto_list'));
    }




    public function nueva_entrada(Request $request){   

    	$id_prod = $request->input('id_producto');  //cambiar esto
    	$cod_barra = $request->input('cod_barra');
        $movimiento = $request->input('movimiento');
    	$id_bodega = $request->input('bodega');
        $id_proveedor= $request->input('id_proveedor');
    	$tipo_doc = $request->input('tipo_doc');
    	$unidad_med = $request->input('unidad_medida');
        $existencia_ant = $request->input('existencia_i');
        $existencia_f = $request->input('existencia_f');
    	$cantidad = $request->input('cantidad');
    	$precio_compra = $request->input('precio_compra');
    	$precio_venta = $request->input('precio_venta');
        $encargado = $request->input('encargado');

    	$factNum = $request->input('factNum');

        if ($existencia_ant=='' or $existencia_ant==0 or $existencia_ant==null) {
            $existencia_ant = 0;
        }
    	
        //print_r($existencia_ant); exit();

    	if (isset($request)) {
          # save to DB
                    //$tickes = Users::create(['imagePath' => 'storage/'.$name]);
                    /*$insert=DB::insert("insert into entrada (ID_PRODUCTO
                                        , ID_BODEGA
                                        ,ID_PROVEEDOR
                                        ,FECHA_ENTRADA
                                        ,TIPO_DOCUMENTO
                                        ,UNIDAD_MEDIDA
                                        ,CANTIDAD
                                        ,COSTO_COMPRA
                                        ,COSTO_VENTA
                                        ,RESPONSABLE)
                        values ('$id_prod','$id_bodega','1',NOW(),'$tipo_doc','$unidad_med','$cantidad','$precio_compra','$precio_venta','$encargado')");*/

                    $insert=DB::insert("INSERT INTO inventario (ID_BODEGA, ID_PRODUCTO, ID_PROVEEDOR, MOVIMIENTO, UNIDAD, EXISTENCIA, EXISTENCIA_ANTE,CANTIDAD,DOCUMENTO,NUM_FACT,RESPONSABLE,FECHA_INVENTARIO)
                                values ( $id_bodega,
                                         $id_prod,
                                         $id_proveedor,
                                        '$movimiento',
                                        '$unidad_med',
                                        '$existencia_f',
                                        '$existencia_ant',
                                        '$cantidad',
                                        '$tipo_doc',
                                        '$factNum',
                                        '$encargado',
                                        NOW())");

                    $edit=DB::update("update PRODUCTO SET EXISTENCIA='$existencia_f' WHERE ID_PRODUCTO=$id_prod");
                    //print_r($name);
                    if ($insert) {
                        return redirect(action('EntradaController@index'))->with('status','!El registro se ingreso con exito!');
                    }else{
                        echo "Error al insertar registro!!";
                    }
                    
                
        }else {
            echo "Error al insertar";
        }

    }


    public static function movimientos($value='')
    {
        $inv=DB::select("SELECT A.*,B.NOMBRE_TIPO_PRODUCTO,C.PROVEEDOR_NOMBRE FROM PRODUCTO A
                                            INNER JOIN TIPO_PRODUCTO B ON A.TIPO_PRODUCTO=B.ID_TIPO_PRODUCTO
                                            INNER JOIN PROVEEDOR C on A.ID_PROVEEDOR=C.ID_PROVEEDOR
                                            WHERE A.INVENTARIO_MINIMO>=A.EXISTENCIA");

            return view('movimientos',compact('inv'));
    }

		/*public function traer_productos()
		{

			//$producto_list=DB::select("SELECT * FROM PRODUCTO");

			//return view('producto',compact('producto_list'));
			
		}*/

        /*if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","La contraseña ingresada no coinside con la actual. Por favor digite nuevamente.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Nueva Password no puede ser igual a la actual password. Por favor digite una diferente.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password se cambio de forma correcta !");*/

    






}
