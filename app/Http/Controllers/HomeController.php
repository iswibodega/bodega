<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $grafprod=DB::select("SELECT B.NOMBRE_TIPO_PRODUCTO,COUNT(*) CANTIDAD,'#0080CC' COLOR FROM PRODUCTO A
                                                                    INNER JOIN TIPO_PRODUCTO B ON A.TIPO_PRODUCTO=B.ID_TIPO_PRODUCTO 
                                                                    GROUP BY  B.NOMBRE_TIPO_PRODUCTO");

        $grafphi=DB::select("SELECT  TIPO_PRODUCTO,COUNT(*) CANTIDAD FROM PRODUCTO GROUP BY TIPO_PRODUCTO");

        return view('home', compact('grafprod','grafphi'));
    }
}
