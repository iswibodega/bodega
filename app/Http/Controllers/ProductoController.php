<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;

class ProductoController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index($value='')
    {
    	# code...
    	$producto_list=DB::select("SELECT A.*,B.NOMBRE_TIPO_PRODUCTO,C.PROVEEDOR_NOMBRE FROM PRODUCTO A
                                            INNER JOIN TIPO_PRODUCTO B ON A.TIPO_PRODUCTO=B.ID_TIPO_PRODUCTO
                                            INNER JOIN PROVEEDOR C on A.ID_PROVEEDOR=C.ID_PROVEEDOR WHERE A.ESTADO_PRODUCTO='ACTIVO'");

			return view('producto',compact('producto_list'));
    }


    public static function lis_producto($value='')
    {
        $prod=DB::select("SELECT ID_PRODUCTO,NOMBRE_PRODUCTO FROM PRODUCTO");

            return  $prod;
    }


     public static function lis_producto_id($id_prod='')
    {
        $prod=DB::select("SELECT * FROM PRODUCTO WHERE ID_PRODUCTO=$id_prod");
            return view('producto_editar',compact('prod'));
            //return  $prod;
    }


     public static function lis_producto_id_mov(Request $request)
    {
        $prod=DB::select("SELECT * FROM PRODUCTO WHERE ID_PRODUCTO='$request->id_prod'");
            //return view('producto_editar',compact('prod'));
            //return  $prod;
        $json_array= array('prod' => $prod );

        return response()->json($json_array);
    }



      public static function inventario_minimo($value='')
    {
        $inv=DB::select("SELECT A.*,B.NOMBRE_TIPO_PRODUCTO,C.PROVEEDOR_NOMBRE FROM PRODUCTO A
                                            INNER JOIN TIPO_PRODUCTO B ON A.TIPO_PRODUCTO=B.ID_TIPO_PRODUCTO
                                            INNER JOIN PROVEEDOR C on A.ID_PROVEEDOR=C.ID_PROVEEDOR
                                            WHERE A.INVENTARIO_MINIMO>=A.EXISTENCIA");

            return view('inventario_minimo',compact('inv'));
    }


     public static function inventario_minimo_cant($value='')
    {
        $result=0;
        $invc=DB::select("SELECT COUNT(*) CANTIDAD FROM PRODUCTO A WHERE A.INVENTARIO_MINIMO>=A.EXISTENCIA");

        $result = $resultArray = json_decode(json_encode($invc), true);

       

         return $result[0]['CANTIDAD']; 
    }


     public static function prodductos_prestamo_cant($value='')
    {
        $resultp=0;
        $invp=DB::select("SELECT COUNT(*) CANTIDAD FROM PRESTAMO A WHERE ESTADO=1");

        $resultp = $resultArray = json_decode(json_encode($invp), true);

       

         return $resultp[0]['CANTIDAD']; 
    }


    public function nuevo_producto(Request $request){   

    	$nombre_prod = $request->input('nombre_prod');
    	$desc_prod = $request->input('desc_prod');
    	$proveedor_prod = $request->input('proveedor_prod');
    	$desc_prod = $request->input('desc_prod');
    	$precio_compra = $request->input('precio_compra');
    	$unidad_med = $request->input('unidad_med');
    	$codigo_prod = $request->input('codigo_prod');
    	$tipo_prod = $request->input('tipo_prod');
    	$inventario_min = $request->input('inventario_min');
    	$precio_venta = $request->input('precio_venta');
    	//$precio_venta = $request->input('precio_venta');

    	//print_r($codigo_prod); exit();

    	if ($request->hasFile('imagen_prod')) {
		    if($request->file('imagen_prod')->isValid()) {
		        try {
		            $file = $request->file('imagen_prod');
		            $name = $codigo_prod.'_'.rand(11111, 99999) . '.' . $file->getClientOriginalExtension();

		            # save to DB
		            //$tickes = Users::create(['imagePath' => 'storage/'.$name]);
		            DB::insert("insert into producto (COD_PRODUCTO, NOMBRE_PRODUCTO,DESC_PRODUCTO,IMAGEN_PRODUCTO,INVENTARIO_MINIMO,TIPO_PRODUCTO,ID_PROVEEDOR,UNIDAD_MEDIDA,PRECIO_COMPRA,PRECIO_VENTA,ID_USUARIO,FECHA_CREACION,ESTADO_PRODUCTO)             							values ('$codigo_prod','$nombre_prod','$desc_prod','$name','$inventario_min','$tipo_prod','$proveedor_prod','$unidad_med','$precio_compra','$precio_venta','1',NOW(),'ACTIVO')");
		            print_r($name);
		            $request->file('imagen_prod')->move("storage/productos", $name);

                    //$message="Registered successfully";
                    //return Redirect::to('producto',compact('message'));
                   // return view('producto_nuevo',compact('message'));
                    return back()->with('success','Producto Creado con Exito!');

		        } catch (Illuminate\Filesystem\FileNotFoundException $e) {

		        		}
		    		}
		}
    }


        public function editar_producto(Request $request){   

        $nombre_prod = $request->input('nombre_prod');
        $desc_prod = $request->input('desc_prod');
        $proveedor_prod = $request->input('proveedor_prod');
        $desc_prod = $request->input('desc_prod');
        $precio_compra = $request->input('precio_compra');
        $unidad_med = $request->input('unidad_med');
        $codigo_prod = $request->input('codigo_prod');
        $tipo_prod = $request->input('tipo_prod');
        $inventario_min = $request->input('inventario_min');
        $precio_venta = $request->input('precio_venta');
        $id_producto = $request->input('idproducto');


        //print_r($codigo_prod); exit();

        //if ($request->hasFile('imagen_prod')) {
          //  if($request->file('imagen_prod')->isValid()) {
             //   try {
                  //  $file = $request->file('imagen_prod');
                   // $name = $nombre_prod.'_'.rand(11111, 99999) . '.' . $file->getClientOriginalExtension();

                    # save to DB
                    //$tickes = Users::create(['imagePath' => 'storage/'.$name]);
                    //DB::insert("insert into producto (COD_PRODUCTO, NOMBRE_PRODUCTO,DESC_PRODUCTO,IMAGEN_PRODUCTO,INVENTARIO_MINIMO,TIPO_PRODUCTO,ID_PROVEEDOR,UNIDAD_MEDIDA,PRECIO_COMPRA,PRECIO_VENTA,ID_USUARIO,FECHA_CREACION,ESTADO_PRODUCTO)                                      values ('$codigo_prod','$nombre_prod','$desc_prod','$name','$inventario_min','$tipo_prod','$proveedor_prod','$unidad_med','$precio_compra','$precio_venta','1',NOW(),'ACTIVO')");
                    //print_r($name);
                    $edit=DB::update("update PRODUCTO SET COD_PRODUCTO='$codigo_prod',NOMBRE_PRODUCTO='$nombre_prod',DESC_PRODUCTO='$desc_prod',INVENTARIO_MINIMO='$inventario_min',TIPO_PRODUCTO='$tipo_prod',ID_PROVEEDOR='$proveedor_prod',UNIDAD_MEDIDA='$unidad_med',PRECIO_COMPRA='$precio_compra',PRECIO_VENTA='$precio_venta',FECHA_MODIFICACION=NOW() WHERE ID_PRODUCTO=$id_producto ");
                   // $request->file('imagen_prod')->move("storage", $name);
               // } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                       // }
                    //}
                    if ($edit) {

                         return redirect(action('ProductoController@index'))->with('status','!El ticket  ha sido actualizado!');
                        # code...

                    }else{

                    }
        }


         public function eliminar_producto($id_prod=''){   
            // $id_producto = $request->input('id_producto');
               $edit=DB::update("update PRODUCTO SET ESTADO_PRODUCTO='INACTIVO' WHERE ID_PRODUCTO='$id_prod' ");

              // print_r($id_prod); die();   

               if ($edit) {

                         return redirect(action('ProductoController@index'))->with('success','!Producto Eliminado con Exito!!');

                          //return back()->with('success','Producto Creado con Exito!');
                        # code...

                    }else{

                          return redirect(action('ProductoController@index'))->with('error','!Error al elimar este Producto!!');

                    }


         }

    //}

		/*public function traer_productos()
		{

			//$producto_list=DB::select("SELECT * FROM PRODUCTO");

			//return view('producto',compact('producto_list'));
			
		}*/

        /*if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","La contraseña ingresada no coinside con la actual. Por favor digite nuevamente.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Nueva Password no puede ser igual a la actual password. Por favor digite una diferente.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password se cambio de forma correcta !");*/

    






}
