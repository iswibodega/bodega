<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;


class ReporteController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

 	 public function index($value='')
    {
    	# code...
   
    		//$producto_list='';
			return view('reportes.reporte_p');
    }



      static public function producto_existencia(Request $request)
    {
    	# code...
    	$fecha_ini= date("Y-m-d h:m:i", strtotime($request->input('fecha_ini')));
    	$fecha_fin= date("Y-m-d h:m:i", strtotime($request->input('fecha_fin'))); 

    	//print_r($fecha_fin); exit();

    	$producto_list=DB::select("SELECT A.*,B.NOMBRE_TIPO_PRODUCTO,C.PROVEEDOR_NOMBRE FROM PRODUCTO A
                                            INNER JOIN TIPO_PRODUCTO B ON A.TIPO_PRODUCTO=B.ID_TIPO_PRODUCTO
                                            INNER JOIN PROVEEDOR C on A.ID_PROVEEDOR=C.ID_PROVEEDOR
                                            WHERE A.FECHA_CREACION BETWEEN '$fecha_ini' AND  '$fecha_fin' ");

			return view('reportes.reporte_p',compact('producto_list'));
    }


}
