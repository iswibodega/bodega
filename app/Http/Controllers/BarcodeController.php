<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Milon\Barcode\DNS1D;
use Auth;


class BarcodeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $grafprod=DB::select("SELECT COD_PRODUCTO,NOMBRE_PRODUCTO FROM PRODUCTO");
        foreach ( $grafprod as $key ) {
           // print_r($key->COD_PRODUCTO); die();
               Storage::disk('local')->put('img_'.$key->COD_PRODUCTO.'.png',base64_decode( DNS1D::getBarcodePNG($key->COD_PRODUCTO, 'C128',2,50)));

            # code...
        }
        
    
        

        return view('barcode', compact('grafprod'));
    }
}
