<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class PrestamoController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index($value='')
    {
    	# code...
    	$producto_list=DB::select("SELECT A.*,B.NOMBRE_PRODUCTO FROM PRESTAMO A INNER JOIN PRODUCTO B ON A.ID_PRODUCTO=B.ID_PRODUCTO");

			return view('salida',compact('producto_list'));
    }

    
    public function nueva_salida(Request $request){   

    	$id_prod = $request->input('id_producto');  //cambiar esto
        $cod_barra = $request->input('cod_barra');
        $id_bodega = $request->input('bodega');
        $motivo= $request->input('motivo');
        $tipo_doc = $request->input('tipo_doc');
        $unidad_med = $request->input('unidad_medida');
        $existencia_ant = $request->input('existencia_i');
        $existencia_f = $request->input('existencia_f');
        $cantidad = $request->input('cantidad');
        $encargado = $request->input('encargado');


    	if (isset($request)) {
            
                
                    $insert=DB::insert("INSERT INTO prestamo (ID_PRODUCTO,ID_BODEGA, CANTIDAD, EXISTENCIA_ANT, DESCRIPCION, RESPONSABLE  ,ESTADO,FECHA_PRESTAMO,CREATED_AT,UPDATED_AT)
                                values ($id_prod,
                                         $id_bodega,                                         
                                         '$cantidad',
                                         '$existencia_ant',
                                        '$motivo',
                                        '$encargado',
                                        '1',
                                        NOW(),                                                                               
                                        NOW(), NOW())");

                    $edit=DB::update("update PRODUCTO SET EXISTENCIA='$existencia_f' WHERE ID_PRODUCTO=$id_prod");
                    //print_r($name);
                    if ($insert) {
                        return redirect(action('PrestamoController@index'))->with('status','!El registro se ingreso con exito!');
                    }else{
                        echo "Error al insertar registro!!";
                    }
                    
                
        }else {
            echo "Error al insertar";
        }


    }



    public function producto_prestamo($value='')
    {
        # code...
        $producto_p=DB::select("SELECT A.*,B.NOMBRE_PRODUCTO FROM PRESTAMO A INNER JOIN PRODUCTO B ON A.ID_PRODUCTO=B.ID_PRODUCTO 
                                                                WHERE ESTADO='1'");

            return view('prestamo_prod',compact('producto_p'));
    }




    public function devolver_producto_prestado(Request $request)
    {
        # code...
        $id_prestamo = $request->input('id_prestamo');    
        $id_prod = $request->input('idprod');     
        $cant = $request->input('cantidad'); 
        $observ ='';
        $observ = trim($request->input('observ'));


        $producto_p=DB::update("UPDATE PRESTAMO SET ESTADO='2', OBSERVACION='$observ', FECHA_DEVOLUCION=NOW(),UPDATED_AT=NOW() WHERE  ID_PRESTAMO=$id_prestamo AND ESTADO='1'");


        $edit=DB::update("update PRODUCTO SET EXISTENCIA=EXISTENCIA+$cant WHERE ID_PRODUCTO=$id_prod");
                    //print_r($name);
                    if ($producto_p) {
                        return redirect(action('PrestamoController@index'))->with('status','!El registro se ingreso con exito!');
                    }else{
                        echo "Error al insertar registro!!";
                    }

            //return view('prestamo_prod',compact('producto_p'));
    }








}
