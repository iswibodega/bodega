<?php
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return  Redirect::action('HomeController@index');
    //return  view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/** Rutas de la aplicacion */
Route::get('/producto_nuevo', function () {
    return view('producto_nuevo');
});


Route::get('/proveedor_editar', function () {
    return view('proveedor_editar');
});


Route::get('/proveedor_nuevo', function () {
    return view('proveedor_nuevo');
});


Route::get('/entrada_nueva', function () {
    return view('entrada_nueva');
});


Route::get('/salida_nueva', function () {
    return view('salida_nueva');
});

/******roles****/
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');
/*****end roles ****/


Route::get('/movimiento', 'ProductoController@lis_producto_id_mov');


Route::get('/producto', 'ProductoController@index');
Route::get('/proveedor', 'ProveedorController@index');
Route::get('/entrada', 'EntradaController@index');
Route::get('/salida', 'PrestamoController@index');
Route::get('/reporte', 'ReporteController@index');

Route::get('/reportem', function () {
    return view('reportes.reporte_r');
});




Route::get('/inventario_min', 'ProductoController@inventario_minimo');
Route::get('/prestamo', 'PrestamoController@producto_prestamo');
Route::get('/movimientos', 'EntradaController@movimientos');


Route::get('editar_prod/{idprod}',['uses' => 'ProductoController@lis_producto_id', 'as' => 'editar_prod']);

Route::get('eliminar_prod/{idprod}',['uses' => 'ProductoController@eliminar_producto', 'as' => 'eliminar_prod']);





Route::post('nuevoProducto', ['uses' => 'ProductoController@nuevo_producto', 'as' => 'nuevoProducto']);
Route::post('nuevoProveedor', ['uses' => 'ProveedorController@nuevo_proveedor', 'as' => 'nuevoProveedor']);
Route::post('nuevaEntrada', ['uses' => 'EntradaController@nueva_entrada', 'as' => 'nuevaEntrada']);
Route::post('nuevaSalida', ['uses' => 'PrestamoController@nueva_salida', 'as' => 'nuevaSalida']);
Route::post('devolucion', ['uses' => 'PrestamoController@devolver_producto_prestado', 'as' => 'devolucion']);
Route::post('ReporteProd', ['uses' => 'ReporteController@producto_existencia', 'as' => 'ReporteProd']);


Route::post('editarProducto', ['uses' => 'ProductoController@editar_producto', 'as' => 'editarProducto']);
//Route::post('editarProducto', ['uses' => 'ProductoController@editar_producto', 'as' => 'editarProducto']);


Route::get('/barcode', 'BarcodeController@index');


/*** ejemplo styde ***/
Route::get('notes', 'NotesController@index');
//Route::get('notes/{id}/destroy', 'NotesController@destroy')->name('notes.destroy');


Route::group(['middleware' => ['permission:destroy_notes']], function () {
    Route::get('notes/{id}/destroy', 'NotesController@destroy')->name('notes.destroy');
});


Route::get('/create_role_permission',function()
{		
	$role = Role::create(['name' => 'Administrator']);
	$permission = Permission::create(['name' => 'Administer_roles_permissions']);
	auth()->user()->assignRole('Administrator');
	auth()->user()->givePermissionTo('Administer_roles_permissions');
});