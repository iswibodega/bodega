
@extends('layouts.master')

@section('content')

 <!-- Content Header (Page header) -->
  <!-- Main content -->
  <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Producto</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    
         
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detalle de productos</h3>
            </div>
                <!-- /.card-header -->
            <div class="card-body" style="font-size: 13px;">             

                <div class="pull-right">
               
                 <a href="{{ url('/producto_nuevo') }}"> <button type="button" class="btn btn-block btn-success"> <i class="fa fa-plus-square"></i> Ingresar Nuevo</button></a>

                </div>

               <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
                  <th>CODIGO</th>
                  <th>NOMBRE</th>
                  <th>TIPO</th>
                  <th>PROVEEDOR</th>
                  <th>PRECIO VENTA</th>
                  <th>PRECIO COMPRA</th>
                  <th>EXISTENCIA</th>
                  <th>IMAGEN</th>
                  <th>ACCIONES</th>
                </tr>
                </thead>
                <tbody>             
                
             @foreach($inv as $prod)
                <tr>
                  <td>{!! $prod->COD_PRODUCTO !!}</td>
                  <td>{!! $prod->NOMBRE_PRODUCTO !!}</td>
                  <td>{!! $prod->NOMBRE_TIPO_PRODUCTO !!}</td>
                  <td>{!! $prod->PROVEEDOR_NOMBRE !!}</td>
                  <td align="center">$ {!! $prod->PRECIO_VENTA !!}</td>
                  <td align="center">$ {!! $prod->PRECIO_COMPRA !!}</td>
                  <td align="center">{!! $prod->EXISTENCIA !!}</td>
                  

                   <td ><button type="button" data-toggle="modal" data-target=".bd-example-model-lg" onclick="document.getElementById('lect_pdf').src='storage/{!! $prod->IMAGEN_PRODUCTO !!}' "><img src="storage/{!! $prod->IMAGEN_PRODUCTO !!}" class="img-responsive img-rounded"
                   style="max-height: 40px; max-width: 40px;"></button></td>

                  <td> <a href="{{ route('editar_prod', ['idproducto' => $prod->ID_PRODUCTO ]) }}"> <button type="button" class="btn btn-warning btn-sm"> <i class="fa fa-edit"></i> Editar</button></a>  <a href="#"> <button type="button" class="btn btn-danger btn-sm" onclick="eliminar_afiliado(<?=$prod->ID_PRODUCTO ?>);"><i class="fa fa-trash-o"></i><font><font class=""> Eliminar </font></font></button></a></td>
                </tr>
              @endforeach
              </tbody>
                <tfoot>
                <tr>
                  <th>CODIGO</th>
                  <th>NOMBRE</th>
                  <th>TIPO</th>
                  <th>PROVEEDOR</th>
                  <th>PRECIO VENTA</th>
                  <th>PRECIO COMPRA</th>
                  <th>EXISTENCIA</th>
                  <th>IMAGEN</th>
                  <th>ACCIONES</th>
                </tr>
                </tfoot>
              </table>
            
          
          <!-- /.box -->
        </div>

<!-- Small modal -->
          <div class="modal fade bd-example-model-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="tue" >
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <img src="#" id="lect_pdf" height="500">
                
              </div>
              
            </div>
          </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

 </section>


@endsection



@section('js')


<!-- DataTables -->
<script src="{{ url('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection