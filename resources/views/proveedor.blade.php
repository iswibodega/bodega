
@extends('layouts.master')

@section('content')

 <!-- Content Header (Page header) -->
  <!-- Main content -->
  <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    
         
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado de proveedores</h3>
            </div>
                <!-- /.card-header -->
            <div class="card-body">             

                <div class="pull-right">
               
                 <a href="{{ url('/proveedor_nuevo') }}"> <button type="button" class="btn btn-block btn-success"> <i class="fa fa-plus-square"></i> Ingresar Nuevo</button></a>

                </div>

               <table id="example1" class="table table-bordered table-striped" style="font-size: 13px;">
              
                <thead>
                <tr>
                  <th>NOMBRE</th>
                  <th>DIRECCION</th>
                  <th>CONTACTO</th>
                  <th>TELEFONO</th>
                  <th>EMAIL</th>
                  <th>ACCIONES</th>
                </tr>
                </thead>
                <tbody>             
                
             @foreach($proveedor_list as $prod)
                <tr>
                  <td>{!! $prod->PROVEEDOR_NOMBRE !!}</td>
                  <td>{!! $prod->PROVEEDOR_DIRECCION !!}</td>
                  <td>{!! $prod->PROVEEDOR_CONTACTO !!}</td>
                  <td>{!! $prod->PROVEEDOR_TELEFONO !!}</td>
                  <td>{!! $prod->PROVEEDOR_CORREO !!}</td>
                  <td> <a href="{{ url('/proveedor_editar') }}"> <button type="button" class="btn btn-warning btn-sm" style="font-size: 10px;"> <i class="fa fa-edit"></i> Editar</button></a>   <a href="{{ url('/producto_nuevo') }}"> <button type="button" class="btn btn-danger btn-sm" style="font-size: 10px;"> <i class="fa  fa-ban"></i> Eliminar</button></a></td>
                </tr>
              @endforeach
              </tbody>
                <tfoot>
                <tr>
                 <th>NOMBRE</th>
                  <th>DIRECCION</th>
                  <th>CONTACTO</th>
                  <th>TELEFONO</th>
                  <th>EMAIL</th>
                  <th>ACCIONES</th>
                </tr>
                </tfoot>
              </table>
            
          
          <!-- /.box -->
        </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

 </section>


@endsection



@section('js')


<!-- DataTables -->
<script src="{{ url('bower_components/DataTables/datatables.min.js') }}"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection