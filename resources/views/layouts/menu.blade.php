     

<!-- Sidebar Menu -->
<nav class="mt-2">
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="font-size: 15px;">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="{{ url('/home') }}" class="nav-link">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="{{ url('/producto') }}" class="nav-link">
              <i class="nav-icon fa fa-cubes"></i>
              <p>
                Productos
               
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/entrada') }}" class="nav-link">
              <i class="nav-icon fa fa-history"></i>
              <p>
                Movimientos de Productos
                
              </p>
            </a>
          </li>
           
          <li class="nav-item">
            <a href="{{ url('/salida') }}" class="nav-link">
              <i class="nav-icon fa  fa-sign-out"></i>
              <p>
                Prestamo de Producto
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/proveedor') }}" class="nav-link">
              <i class="nav-icon fa fa-truck"></i>
              <p>
               Proveedores
                
              </p>
            </a>
          </li>

           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Reportes
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/reporte') }}" class="nav-link">
                  <i class="fa fa-file-text-o nav-icon"></i>
                  <p>Reportes General</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ url('/reportem') }}" class="nav-link">
                  <i class="fa fa-bar-chart nav-icon"></i>     
                  <p>Reportes de Movimientos</p>
                </a>
              </li>
                <li class="nav-item">
                <a href="{{ url('/reporte') }}" class="nav-link">
                  <i class="fa fa-calendar-check-o nav-icon"></i>
                  <p>Reportes de Prestamos</p>
                </a>
              </li>
             
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-edit"></i>
              <p>
                Usuarios
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/users') }}" class="nav-link">
                  <i class="fa fa-users nav-icon"></i>
                  <p>Lista de Usuarios</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ url('/roles') }}" class="nav-link">
                  <i class="fa fa-unlock-alt nav-icon"></i>
                  <p>Roles</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ url('/permissions') }}" class="nav-link">
                  <i class="fa fa-key nav-icon"></i>
                  <p>Permisos</p>
                </a>
              </li>
             
            </ul>
          </li>
         
       
            @if (Route::has('login'))
                   
                        @auth
                            <li><a href="{{ url('/home') }}"><span></span></a></li>
                        @else
                            <li><a href="{{ route('login') }}"><span>Login</span></a></li>
                            <li><a href="{{ route('register') }}"><span>Register</span></a></li>
                        @endauth
                    
                @endif

        </ul>
      </nav>

