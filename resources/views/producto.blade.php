
@extends('layouts.master')

@section('content')

 <!-- Content Header (Page header) -->
  <!-- Main content -->
  <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">
      <!-- /.content-header -->
         
        <div class="card" style="margin-top: 5px;">
            <div class="card-header">
              <h3 class="card-title">Detalle de productos    <div class="pull-right">               
                 <a href="{{ url('/producto_nuevo') }}"> <button type="button" class="btn btn-block btn-success" style="font-size: 15px;"> <i class="fa fa-plus-square"></i> Ingresar Nuevo</button></a>

                </div></h3>
            </div>
                <!-- /.card-header -->

            <div class="card-body" style="font-size: 13px;">             
                             

               <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
                  <th>CODIGO</th>
                  <th>IMAGEN</th>
                  <th>NOMBRE</th>
                  <th>TIPO</th>
                  <th>PROVEEDOR</th>
                 
                  <th>PRECIO COMPRA</th>
                  <th>EXISTENCIA</th>
                  <th>ACCIONES</th>
                </tr>
                </thead>
                <tbody>             
                
             @foreach($producto_list as $prod)
                <tr>
                  <td>{!! $prod->COD_PRODUCTO !!}</td>
                   <td ><button type="button" data-toggle="modal" data-target="#exampleModal" onclick="document.getElementById('lect_pdf').src='storage/productos/{!! $prod->IMAGEN_PRODUCTO !!}' "><img src="storage/productos/{!! $prod->IMAGEN_PRODUCTO !!}" class="img-responsive img-rounded"
                   style="max-height: 40px; max-width: 40px;"></button></td>
                  <td>{!! $prod->NOMBRE_PRODUCTO !!}</td>
                  <td>{!! $prod->NOMBRE_TIPO_PRODUCTO !!}</td>
                  <td>{!! $prod->PROVEEDOR_NOMBRE !!}</td>
                  
                  <td align="center">$ {!! $prod->PRECIO_COMPRA !!}</td>
                  <td align="center"><?php if (  $prod->EXISTENCIA <=  $prod->INVENTARIO_MINIMO) { ?>
                    <span class="text-danger" title="Baja Existencia!"><b> {!! $prod->EXISTENCIA !!}  </b></span>
                  <?php }else{?>   {!! $prod->EXISTENCIA !!} <?php } ?></td>
                  

                  

                  <td> <a href="{{ route('editar_prod', ['idproducto' => $prod->ID_PRODUCTO ]) }}">
                          <button type="button" class="btn btn-warning btn-sm" style="font-size: 10px;"> 
                            <i class="fa fa-edit"></i> 
                              Editar
                          </button>
                        </a>  
                        <a onclick="return confirm('Esta seguro de Eliminar este Producto?')"  href="{{ route('eliminar_prod', ['idproducto' => $prod->ID_PRODUCTO ]) }}">
                           <button type="button" class="btn btn-danger btn-sm" onclick="eliminar_afiliado(<?=$prod->ID_PRODUCTO ?>);" style="font-size: 10px;">
                            <i class="fa fa-trash-o"></i>
                            <font><font class="">Eliminar </font></font>
                           </button>
                         </a>
                  </td>
                </tr>
              @endforeach
              </tbody>
                <tfoot>
                <tr>
                  <th>CODIGO</th>
                   <th>IMAGEN</th>
                  <th>NOMBRE</th>
                  <th>TIPO</th>
                  <th>PROVEEDOR</th>
                  
                  <th>PRECIO COMPRA</th>
                  <th>EXISTENCIA</th>
                  <th>ACCIONES</th>
                </tr>
                </tfoot>
              </table>
            
          
          <!-- /.box -->
        </div>

<!-- Small modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="tue" >
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                    <h7 class="modal-title" id="exampleModalLabel">Imagen de Producto</h7>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                <img src="#" id="lect_pdf" width="100%">
                
              </div>
              
            </div>
          </div>


        
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

 </section>


@endsection



@section('js')


<!-- DataTables -->

<script src="{{ url('bower_components/DataTables/datatables.min.js') }}"></script>


<script>
  $(function () {
    $('#example1').DataTable({
               "lengthMenu": [10,25],
          dom: 'Bfrtip',
          buttons: ['copy','excel','pdf'],           
           "language": idioma_español
         
    });
    
  });

  var idioma_español= {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}

</script>

@endsection