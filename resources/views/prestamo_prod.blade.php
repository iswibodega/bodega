
@extends('layouts.master')

@section('content')

 <!-- Content Header (Page header) -->
  <!-- Main content -->
  <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Prestamo de Equipo</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    
         
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado de Prestamos</h3>
            </div>
                <!-- /.card-header -->
            <div class="card-body" style="font-size: 13px;">             

                <div class="pull-right">
               
                 <a href="{{ url('/salida_nueva') }}"> <button type="button" class="btn btn-block btn-success"> <i class="fa fa-plus-square"></i> Ingresar Nuevo</button></a>

                </div>

               <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
                  <th>FECHA_REGISTRO</th>
                 <th>PRODUCTO</th>
                 <th>MOTIVO</th>
                  <th>RESPONSABLE</th>
                  <th>ESTADO</th>
                  <th>BODEGA</th>
                  <th>CANTIDAD</th>
                  <th>OBSERV</th>
                  <th>FECHA_DEVOLUCION</th>
                  <th>ENTREGA</th>
                </tr>
                </thead>
                <tbody>             
                <?php //print_r($producto_list); exit();
                ?>
             @foreach($producto_p as $prod)
                <tr>
                  <td>{!! $prod->FECHA_PRESTAMO !!}</td>
                  <td>{!! $prod->NOMBRE_PRODUCTO !!}</td>
                  <td>{!! $prod->DESCRIPCION !!}</td>
                  <td>{!! $prod->RESPONSABLE !!}</td>
                  <td><?php if ( $prod->ESTADO==1) { ?>
                    <small class="badge badge-warning" style="font-size: 11px"><i class="fa fa-clock-o"></i> Prestado</small>
                 <?php }elseif ( $prod->ESTADO==2) { ?>
                    <small class="badge badge-success" style="font-size: 11px"><i class="fa fa-check"></i> Devuelto</small>
                 <?php } ?></td>
                  <td>{!! $prod->ID_BODEGA !!}</td>
                  <td>{!! $prod->CANTIDAD !!}</td>
                  <td>{!! $prod->OBSERVACION !!}</td>
                  <td>{!! $prod->FECHA_DEVOLUCION !!}</td>
                  <td><button>Devolver</button></td>
                 
                  
                </tr>
              @endforeach
              </tbody>
                <tfoot>
                <tr>
                  <th>FECHA_REGISTRO</th>
                 <th>PRODUCTO</th>
                 <th>MOTIVO</th>
                  <th>RESPONSABLE</th>
                  <th>ESTADO</th>
                  <th>BODEGA</th>
                  <th>CANTIDAD</th>
                  <th>OBSERV</th>
                  <th>FECHA_DEVOLUCION</th>
                  <th>ENTREGA</th>
                  
                </tr>
                </tfoot>
              </table>
            
          
          <!-- /.box -->
        </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

 </section>


@endsection



@section('js')


<!-- DataTables -->
<script src="{{ url('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection