
@extends('layouts.master')

@section('content')

<!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    
    <!-- /.content-header -->

    <!-- Main content -->
       <div class="card card-default" style="margin-top: 5px;">
            <div class="card-header">
               <h3 class="card-title">Editar Producto</h3>

               @foreach($prod as $data)
                 {{-- $data->ID_PRODUCTO --}}
              @endforeach
            </div>
                <!-- /.card-header -->

             <?php //print_r($data); die();{!! $prod->IMAGEN_PRODUCTO 
             //$rut_img='storage/productos/'.$data->IMAGEN_PRODUCTO;
             //print_r($rut_img); die();
              ?>
         <!-- /.box-header -->
        <div class="card-body">
          
            <form class="form" method="POST" action="{{ route('editarProducto') }}"  enctype="multipart/form-data">
               {{ csrf_field() }}

               <div class="row">
               
                <div class="col-md-6">
                
                  <img src="{{ url('storage/productos/'.$data->IMAGEN_PRODUCTO) }}" class="img-responsive img-rounded"
                   style="max-height: 150px; max-width: 150px;">
                   <div class="form-group">
                    <label>Cambiar Imagen</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                      </div>
                      <input type="file" name="imagen_prod" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                     
                    </div>
                  </div>
                   <hr>
                  
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Proveedor</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-truck"></i></span>
                      </div>
                      <select class="form-control" name="proveedor_prod" data-mask="">
                        <option value="0">Seleccionar..</option>
                        <?php $prod= \App\Http\Controllers\ProveedorController::lis_proveedor(); ?>

                        @foreach($prod as $pro)
                            <option value="{{ $pro->ID_PROVEEDOR }}">{{ $pro->PROVEEDOR_NOMBRE }}</option>
                            <?php if ($pro->ID_PROVEEDOR== $data->ID_PROVEEDOR) {?>
                               <option value="{{ $pro->ID_PROVEEDOR }}" selected="true">{{ $pro->PROVEEDOR_NOMBRE }}</option>
                           <?php } ?>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Precio de Compra</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-dollar"></i></span>
                      </div>
                      <input type="text" name="precio_compra" class="form-control" data-mask="" value="{{ $data->PRECIO_COMPRA }}">
                    </div>
                  </div>
                  <!-- /.form-group -->
                   <div class="form-group">
                    <label>Unidad de Medida</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-balance-scale"></i></span>
                      </div>
                      <input type="text" name="unidad_med" class="form-control" data-mask=""  value="{{ $data->UNIDAD_MEDIDA }}">
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Nombre Producto</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-pencil-square-o"></i></span>
                       
                      </div>
                      <input type="text" name="nombre_prod" class="form-control"  data-mask="" value="{{ $data->NOMBRE_PRODUCTO }}">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Descripcion</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-newspaper-o"></i></span>
                      </div>
                      <input type="text" name="desc_prod" class="form-control" data-mask="" value="{{ $data->NOMBRE_PRODUCTO }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Codigo</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-barcode"></i></span>
                      </div>
                      <input type="text" name="codigo_prod" class="form-control" data-mask="" value="{{ $data->COD_PRODUCTO }}">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Tipo de Producto</label>
                   <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-bars"></i></span>
                      </div>
                      <select class="form-control" name="tipo_prod" data-mask="">
                        <?php if ( $data->TIPO_PRODUCTO ==1) { ?>
                           <option value="1" selected="true">Herrameintas</option>
                       <?php }elseif ($data->TIPO_PRODUCTO ==2) { ?>
                          <option value="2" selected="true">Accesorios</option>
                       <?php } ?>
                        <option value="0">Seleccionar..</option>
                        <option value="1">Herramientas</option>
                        <option value="2">Oficina</option>
                        <option value="3">Accesorios</option>
                        <option value="4">Insumos</option>
                      </select>
                      
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Inventario Minimo</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa  fa-battery-1"></i></span>
                      </div>
                      <input type="text" name="inventario_min" class="form-control" data-mask=""  value="{{ $data->INVENTARIO_MINIMO }}">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Precio de Venta</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-dollar"> </i></span>
                      </div>
                      <input type="text" name="precio_venta" class="form-control" data-mask="" value="{{ $data->PRECIO_VENTA }}">
                    </div>
                  </div>
                  <!-- /.form-group -->
                   <!-- /.form-group -->
                 <!-- <div class="form-group">
                    <label>Imagen de Producto</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                      </div>
                      <input type="file" name="imagen_prod" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                     
                    </div>
                  </div>-->
                <!-- /.form-group -->
                 
                </div>
                <div class="col-md-12" style="text-align: center;">
                <div class="form-group">
                    <div class="btn-group">
                    <input type="hidden"  name="idproducto" value=" {{ $data->ID_PRODUCTO }}">
                  <button type="submit" class="btn btn-block btn-success"> <i class="fa fa-save"></i> <b>Guardar</b></button></div>
                  <div class="btn-group"><button type="button" class="btn btn-block btn-warning" onclick="reset()"> <i class="fa fa-eraser"></i> <b>Cancelar</b></button></div>
                  <div class="btn-group"><button type="button" class="btn btn-block btn-primary" onclick="history.back()"><i class="fa fa-undo"></i> <b>Regresar</b></button></div>
                </div>
                <!-- /.col -->
              </div>

              </div>
          </form>
          
        </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>


@endsection
