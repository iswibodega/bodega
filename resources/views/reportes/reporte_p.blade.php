
@extends('layouts.master')

@section('content')

 <!-- Content Header (Page header) -->
  <!-- Main content -->
  <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          
        
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    
         
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detalle de productos</h3>
            </div>
                <!-- /.card-header -->
            <div class="card-body" style="font-size: 13px;">             


             <form class="form" method="POST" action="{{ route('ReporteProd') }}" >
               {{ csrf_field() }}
               <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Fecha Inicial</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa  fa-pencil-square-o"></i></span>
                      </div>
                      <input type="text" class="form-control float-right" id="fecha_ini" name="fecha_ini"  data-date-format="mm/dd/yyyy">
                    </div>
                  </div>
                 
                  
                 
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Fecha Final</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa fa-barcode"></i></span>
                      </div>
                      <input type="text" class="form-control float-right" id="fecha_fin"  name="fecha_fin" data-date-format="mm/dd/yyyy">
                    </div>
                  </div>
                  
                 
                 
                </div>
                <div class="col-md-1" style="text-align: center;">
                <div class="form-group">
                    <div class="btn-group">
                  <button type="submit" class="btn btn-block btn-success btn"> <i class="fa fa-save"></i> Guardar</button></div>
                  <div class="btn-group"></div>
                  
                </div>
                <!-- /.col -->
              </div>
               </div>
          <!-- /.row -->
          </form>


                  

                <?php if (isset($producto_list)) { ?>
                  
              

                <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
                  <th>CODIGO</th>
                  <th>NOMBRE</th>
                  <th>TIPO</th>
                  <th>PROVEEDOR</th>
                  <th>PRECIO VENTA</th>
                  <th>PRECIO COMPRA</th>
                  <th>EXISTENCIA</th>
                  <th>IMAGEN</th>
                  <th>ACCIONES</th>
                </tr>
                </thead>
                <tbody>             
                 
             @foreach($producto_list as $prod)
                <tr>
                  <td>{!! $prod->COD_PRODUCTO !!}</td>
                  <td>{!! $prod->NOMBRE_PRODUCTO !!}</td>
                  <td>{!! $prod->NOMBRE_TIPO_PRODUCTO !!}</td>
                  <td>{!! $prod->PROVEEDOR_NOMBRE !!}</td>
                  <td align="center">$ {!! $prod->PRECIO_VENTA !!}</td>
                  <td align="center">$ {!! $prod->PRECIO_COMPRA !!}</td>
                  <td align="center">{!! $prod->EXISTENCIA !!}</td>
                  

                   <td ></td>

                  <td> </td>
                </tr>
              @endforeach
              </tbody>
                <tfoot>
                <tr>
                  <th>CODIGO</th>
                  <th>NOMBRE</th>
                  <th>TIPO</th>
                  <th>PROVEEDOR</th>
                  <th>PRECIO VENTA</th>
                  <th>PRECIO COMPRA</th>
                  <th>EXISTENCIA</th>
                  <th>IMAGEN</th>
                  <th>ACCIONES</th>
                </tr>
                </tfoot>
              </table>


              <?php   } ?>
            
          
          <!-- /.box -->
        </div>

<!-- Small modal -->
          <div class="modal fade bd-example-model-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="tue" >
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <img src="#" id="lect_pdf" height="500">
                
              </div>
              
            </div>
          </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

 </section>


@endsection



@section('js')


<!-- DataTables -->

<script src="{{ url('bower_components/DataTables/datatables.min.js') }}"></script>

<script src="{{ url('plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<script>
  $(function () {
    $('#example1').dataTable({         
          dom: 'Bfrtip',
          buttons: ['copy','excel','csv','pdf'],           
           "language": idioma_español,
           'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
         
      });

$('#fecha_ini').datepicker({
    currentText: 'Hoy',
    dateFormat: 'dd/mm/yy'
   });

$('#fecha_fin').datepicker({
    currentText: 'Hoy',
    dateFormat: 'dd/mm/yy'
   });

  });

  var idioma_español= {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}


</script>
@endsection