
@extends('layouts.master')

@section('content')

 <!-- Content Header (Page header) -->
  <!-- Main content -->
  <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
     
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    
         
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado de Prestamos</h3>
            </div>
                <!-- /.card-header -->
            <div class="card-body" style="font-size: 13px;">             

                <div class="pull-right">
               
                 <a href="{{ url('/salida_nueva') }}"> <button type="button" class="btn btn-block btn-success"> <i class="fa fa-plus-square"></i> Ingresar Nuevo</button></a>

                </div>

               <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
                  <th>FECHA_PRESTAMO</th>
                 <th>PRODUCTO</th>
                 <th>MOTIVO</th>
                  <th>RESPONSABLE</th>
                  <th>ESTADO</th>
                  <th>BODEGA</th>
                  <th>CANTIDAD</th>
                  <th>FECHA_DEVOLUCION</th>
                  <th>ENTREGA</th>
                </tr>
                </thead>
                <tbody>             
                <?php //print_r($producto_list); exit();
                ?>
             @foreach($producto_list as $prod)
                <tr>
                  <td><?php echo date("d/m/Y", strtotime($prod->FECHA_PRESTAMO)); ?></td>
                  <td>{!! $prod->NOMBRE_PRODUCTO !!}</td>
                  <td>{!! $prod->DESCRIPCION !!}</td>
                  <td>{!! $prod->RESPONSABLE !!}</td>
                  <td><?php if ( $prod->ESTADO==1) { ?>
                    <small class="badge badge-warning" style="font-size: 11px"><i class="fa fa-clock-o"></i> Prestado</small>
                 <?php }elseif ( $prod->ESTADO==2) { ?>
                    <small class="badge badge-success" style="font-size: 11px"><i class="fa fa-check"></i> Devuelto</small>
                 <?php } ?></td>
                  <td>{!! $prod->ID_BODEGA !!}</td>
                  <td>{!! $prod->CANTIDAD !!}</td>
                  <?php if ( $prod->ESTADO==2) {  ?>
                  <td><?php echo date("d/m/Y", strtotime($prod->FECHA_DEVOLUCION)); ?></td>
                    <?php }else{ ?>
                              <td> </td>
                          <?php } ?>

                  <?php if ( $prod->ESTADO==1) {  ?>
                    <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" onclick="document.getElementById('id_prestamo').setAttribute('value','{!! $prod->ID_PRESTAMO !!}'); document.getElementById('cantidad').setAttribute('value','{!! $prod->CANTIDAD !!}'); document.getElementById('idprod').setAttribute('value','{!! $prod->ID_PRODUCTO !!}')">Entregar</button></td>
                 <?php  }else{  ?>
                          <td></td>
                    
                  <?php } ?>
  
                 
                  
                </tr>
              @endforeach
              </tbody>
                <tfoot>
                <tr>
                  <th>FECHA_PRESTAMO</th>
                 <th>PRODUCTO</th>
                 <th>MOTIVO</th>
                  <th>RESPONSABLE</th>
                  <th>ESTADO</th>
                  <th>BODEGA</th>
                  <th>CANTIDAD</th>
                  <th>FECHA_DEVOLUCION</th>
                  <th>ENTREGA</th>
                </tr>
                </tfoot>
              </table>
            



   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Devolucion de Producto Prestado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_obs" class="form" method="POST" action="{{ route('devolucion') }}">
           {{ csrf_field() }}
           <div class="form-group">
                    <label>Agregar Observación</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-search"></i></span>
                      </div>
                      <textarea name="observ" id="observ" class="form-control" required ></textarea>
                      <input type="hidden" name="id_prestamo" id="id_prestamo" value="">
                      <input type="hidden" name="cantidad" id="cantidad" value="">
                      <input type="hidden" name="idprod" id="idprod" value="">
                    </div>
                  </div>
       
      
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
       </form>
    </div>
  </div>
</div>
          
          <!-- /.box -->
        </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

 </section>


@endsection



@section('js')


<!-- DataTables -->
<script src="{{ url('bower_components/DataTables/datatables.min.js') }}"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection