@extends('layouts.app')

@section('content')
   
<div class="login-box">
   
  
   <div class="card">
    <div class="card-body login-card-body">
        <div class="login-logo">
          <img src="{{ url('dist/img/logo2.png') }}" style="width: 85%;" class="" alt="User Image">
        </div>

      <p class="login-box-msg">Ingrese sus credenciales</p>
                   

        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
        <div class="form-group has-feedback">
            
          <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
         
           @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
        </div>
        <div class="form-group has-feedback">

            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
          
          @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
        </div>
        
        
          <!-- /.col -->
          <div class="col-12" >
            <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-fw fa-sign-in"></i> LogIn</button>

               </div>
          <!-- /.col -->
        
      </form>
                </div>
            </div>
        </div>


@endsection
