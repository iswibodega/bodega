
@extends('layouts.master')

@section('content')

 <!-- Content Header (Page header) -->
  <!-- Main content -->
  <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Reportes</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    
         
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detalle de productos</h3>
            </div>
                <!-- /.card-header -->
            <div class="card-body" style="font-size: 13px;">         


                  
            

                 <table id="example1" class="table table-bordered table-striped">
              
                <thead>
                <tr>
                  <th>BARRA</th>
                  <th>BARRAi</th>
                  <th>CODIGO</th>
                  <th>NOMBRE</th>
          
                </tr>
                </thead>
                <tbody>             
                 
             @foreach($grafprod as $prod)
                  
             <?php $codigo_b = '"'. $prod->COD_PRODUCTO.'"'; 
                   $img_url = 'storage/img_'. $prod->COD_PRODUCTO.'.png';
              /*{!! DNS1D::getBarcodeSVG($codigo_b, "C128") !!}*/?>
                <tr>
                  <td><div>{!! DNS1D::getBarcodeHTML($codigo_b, "C128") !!}</div><div style="padding-top: 20px; width: 24%;">{!! $prod->
                  NOMBRE_PRODUCTO !!}</div> </td>
                    <td ><img src="{!! asset( $img_url) !!}" class="img-responsive" style="max-width: 50%;"></td>
                 <td>{!! $prod->COD_PRODUCTO !!}</td>
                  <td>{!! $prod->NOMBRE_PRODUCTO !!}</td>

                </tr>
              @endforeach
              <style type="text/css">
                .code{
                  height: 50px !important;                 
                }
              </style>

              </tbody>
                <tfoot>
                <tr>
                  <th>BARRA</th>
                  <th>BARRAi</th>
                  <th>CODIGO</th>
                  <th>NOMBRE</th>
                
                </tr>
                </tfoot>
              </table>


            
            
          
          <!-- /.box -->
        </div>

<!-- Small modal -->
          <div class="modal fade bd-example-model-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="tue" >
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <img src="#" id="lect_pdf" height="500">
                
              </div>
              
            </div>
          </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

 </section>


@endsection



@section('js')


<!-- DataTables -->

<script src="{{ url('plugins/DataTables/datatables.min.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap4.js') }}"></script>


<script>
  $(function () {
    $('#example1').dataTable({ 
          "lengthMenu": [10,25],
          dom: 'Bfrtip',
          buttons: ['copy','excel','pdf'],           
           "language": idioma_español
         
      });

  });

  var idioma_español= {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}


</script>
@endsection