
@extends('layouts.master')

@section('content')

<!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

  
    <!-- /.content-header -->

    <!-- Main content -->
       <div class="card card-default"  style="margin-top: 5px;">
            <div class="card-header">
               <h3 class="card-title">Nuevo Producto</h3>
            </div>
                <!-- /.card-header -->
             
         <!-- /.box-header -->
        <div class="card-body">
          
            <form class="form" method="POST" action="{{ route('nuevoProducto') }}"  enctype="multipart/form-data">
               {{ csrf_field() }}

               <div class="row">
               
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Nombre Producto</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-pencil-square-o"></i></span>
                       
                      </div>
                      <input type="text" name="nombre_prod" class="form-control"  data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Descripcion</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-newspaper-o"></i></span>
                      </div>
                      <input type="text" name="desc_prod" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Proveedor</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-truck"></i></span>
                      </div>
                      <select class="form-control" name="proveedor_prod" data-mask="">
                        <option value="0">Seleccionar..</option>
                        <?php $prod= \App\Http\Controllers\ProveedorController::lis_proveedor(); ?>
                        @foreach($prod as $pro)
                            <option value="{{ $pro->ID_PROVEEDOR }}">{{ $pro->PROVEEDOR_NOMBRE }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Precio de Compra</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-dollar"></i></span>
                      </div>
                      <input type="text" name="precio_compra" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                   <div class="form-group">
                    <label>Unidad de Medida</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-balance-scale"></i></span>
                      </div>
                      <input type="text" name="unidad_med" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Codigo</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-barcode"></i></span>
                      </div>
                      <input type="text" name="codigo_prod" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Tipo de Producto</label>
                   <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-bars"></i></span>
                      </div>
                      <select class="form-control" name="tipo_prod" data-mask="">
                        <option value="0">Seleccionar..</option>
                        <option value="1">Herramientas</option>
                        <option value="2">Oficina</option>
                        <option value="3">Accesorios</option>
                        <option value="4">Insumos</option>
                      </select>
                      
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Inventario Minimo</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa  fa-battery-1"></i></span>
                      </div>
                      <input type="text" name="inventario_min" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Precio de Venta</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-dollar"> </i></span>
                      </div>
                      <input type="text" name="precio_venta" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                   <!-- /.form-group -->
                  <div class="form-group">
                    <label>Imagen de Producto</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                      </div>
                      <input type="file" name="imagen_prod" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                     
                    </div>
                  </div>
                  <!-- /.form-group -->
                 
                </div>
                <div class="col-md-12" style="text-align: center;">
                <div class="form-group">
                    <div class="btn-group">
                  <button type="submit" class="btn btn-block btn-success"> <i class="fa fa-save"></i> <b>Guardar </b></button></div>
                  <div class="btn-group"><button type="button" class="btn btn-block btn-warning" onclick="reset()"> <i class="fa fa-eraser"></i> <b>Cancelar</b></button></div>
                  <div class="btn-group"><button type="button" class="btn btn-block btn-danger" onclick="history.back()"><i class="fa fa-undo"></i> <b>Regresar</b></button></div>
                </div>
                <!-- /.col -->
              </div>

              </div>
          </form>
          
        </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>


@endsection

@section('js')

<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js') }}"></script>
 <script src="{{ url('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script type="text/javascript">  

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    });
 
 
</script>


@endsection