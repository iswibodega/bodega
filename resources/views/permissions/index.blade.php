{{-- \resources\views\permissions\index.blade.php --}}

@extends('layouts.master')

@section('content')

 <section class="content">
    <div class="row">
    <div class="col-12">


     <div class="card">
     <div class="card-header">
    <h3><i class="fa fa-key"></i>Permisos Disponibles

    <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
    <a href="{{ route('roles.index') }}" class="btn btn-default pull-right">Roles</a></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $permission->name }}</td> 
                    <td>
                    <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

<div class="pull-right">
      <a href="{{ URL::to('permissions/create') }}" class="btn btn-success"><i class="fa fa-plus-square"></i> Agregar Permiso</a>           
     </div>
     <hr>  

</div>
</div>
</div>
</section>

@endsection
