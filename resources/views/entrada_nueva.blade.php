
@extends('layouts.master')

@section('content')

<!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Entrada</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
       <div class="card card-default">
            <div class="card-header">
               <h3 class="card-title">Nueva Entrada</h3>
            </div>
                <!-- /.card-header -->
             
         <!-- /.box-header -->
        <div class="card-body">
          
            <form class="form" method="POST" action="{{ route('nuevaEntrada') }}"  enctype="multipart/form-data">
               {{ csrf_field() }}

               <div class="row">
               
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Producto</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-pencil-square-o"></i></span>
                       
                      </div>
                      <select class="form-control" id="id_producto" name="id_producto" data-mask=""  required>
                        <option selected="selected" >Seleccionar Producto</option>
                        <?php $prod= \App\Http\Controllers\ProductoController::lis_producto(); ?>
                        @foreach($prod as $pro)
                            <option value="{{ $pro->ID_PRODUCTO }}">{{ $pro->NOMBRE_PRODUCTO }}</option>
                        @endforeach
                        
                      </select>
                    </div>
                  </div>

                   <!-- /.form-group -->
                   <div class="form-group">
                    <label>Tipo de Movimiento</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-home"></i></span>
                      </div>
                      <select class="form-control" name="movimiento" id="movimiento" data-mask="" required>
                        <option value="0">Seleccionar..</option>
                        <option value="1">Entra de Producto</option>
                        <option value="2">Salida de Producto</option>
                        <option value="2">Prestamo</option>
                       
                      </select>
                    </div>
                  </div>

                  
                  <div class="form-group">
                    <label>Existencia Actual</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-check-square-o"></i></span>
                      </div>
                      <input type="text" name="exis_a" id="exis_a" class="form-control" data-mask="" disabled="true">
                    </div>
                  </div>
                  <!-- /.form-group -->

                  <div class="form-group">
                    <label>Precio de Compra</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-dollar"></i></span>
                      </div>
                      <input type="text" name="precio_compra" id="precio_compra" class="form-control" data-mask="" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Bodega</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-home"></i></span>
                      </div>
                      <select class="form-control" name="bodega" data-mask="" required>
                        <option value="0">Seleccionar..</option>
                        <option value="1" selected="true">Bodega 1</option>
                        <option value="2">Bodega 2</option>
                      </select>
                    </div>
                  </div>

                   <div class="form-group" id="numFact" style="display: none;">
                    <label>
                      Numero de Factura
                    </label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                      </div>
                      <input type="text" name="factNum" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <!-- /.form-group -->
                 
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Codigo Barra</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-barcode"></i></span>
                      </div>
                      <input type="text" name="cod_barra" id="cod_barra" class="form-control" data-mask="" required value="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Tipo de Documento</label>
                   <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-folder"></i></span>
                      </div>
                      <select class="tipodoc form-control" name="tipo_doc" id="tipo_doc" data-mask="">
                        <option value="0">Seleccionar..</option>
                        <option value="1">Factura</option>
                        <option value="2">CCF</option>
                        <option value="3">Recibo</option>
                        <option value="4">Requisa</option>
                      </select>
                      
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Cantidad</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa  fa-calculator"></i></span>
                      </div>
                      <input type="text" name="cantidad"  id="cantidad"  class="form-control" data-mask="" required onblur="operar_existencia()">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Precio de Venta</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-dollar"> </i></span>
                      </div>
                      <input type="text" name="precio_venta" id="precio_venta" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->

                   <!-- /.form-group -->
                   <div class="form-group">
                    <label>Encargado</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                      </div>
                      <input type="text" name="encargado" class="form-control" data-mask="">
                    </div>
                  </div>

                  
                   
                 
                </div>
                <div class="col-md-12" style="text-align: center;">
                <div class="form-group">
                    <div class="btn-group">
                      <input type="hidden" name="existencia_i" id="existencia_i" value="">
                      <input type="hidden" name="existencia_f" id="existencia_f" value="">
                      <input type="hidden" name="unidad_medida" id="unidad_medida" value="">
                      <input type="hidden" name="id_proveedor" id="id_proveedor" value="">
                  <button type="submit" class="btn btn-block btn-success btn-lg"> <i class="fa fa-save"></i> Guardar</button></div>
                  <div class="btn-group"><button type="button" class="btn btn-block btn-warning btn-lg" onclick="reset()"> <i class="fa fa-eraser"></i> Cancelar</button></div>
                  <div class="btn-group"><a href="{{ URL::previous() }}"><button type="button" class="btn btn-block btn-primary btn-lg"><i class="fa fa-undo"></i> Regresar</button></a></div>
                </div>
                <!-- /.col -->
              </div>

              </div>
          </form>
          
        </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>


@endsection

@section('js')

<!-- Select2 -->
<!-- Select2 -->
<script src="{{ url('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script type="text/javascript">  

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({
              allowClear: true,
              width: "resolve"
    });
    });

  $(document).ready(function() {
    $('#id_producto').select2({
              allowClear: true,
              width: "resolve"
    });
});
 
 $('#id_producto').change(function() { //on('change','select', function() {
  $value=$(this).val();
  $.ajax({
            type : 'get',
            url  : '{{URL::to('movimiento')}}',
            data : {'id_prod':$value },
            success: function (data) { //alert(data.prod[0].COD_PRODUCTO);
              if (data.length != 0) {
                $("#cod_barra").val(data.prod[0].COD_PRODUCTO);
                $("#precio_venta").val(data.prod[0].PRECIO_VENTA);
                $("#precio_compra").val(data.prod[0].PRECIO_COMPRA);
                $("#exis_a").val(data.prod[0].EXISTENCIA);
                $("#existencia_i").val(data.prod[0].EXISTENCIA);
                $("#unidad_medida").val(data.prod[0].UNIDAD_MEDIDA);
                $("#id_proveedor").val(data.prod[0].ID_PROVEEDOR);
              }
            }
  });
   // body...
 });

function operar_existencia() {
  $mov = $('#movimiento').val();
  $exis_i = parseInt($('#existencia_i').val());
  $cant = parseInt($('#cantidad').val());
  $exis_f='';
  //alert($mov);
  if($mov==1){
    $exis_f= ($cant + $exis_i);
    $('#existencia_f').val($exis_f);
  }else{
    if ($mov==2) {
       $exis_f= ( $exis_i - $cant);
       if ($exis_f <= 0) { 
         alert('La cantidad requrida es mayor a la exitencia del producto, intente con una cantidad menor!');
         $('#cantidad').val('');
       }
    $('#existencia_f').val($exis_f);
    }
  }
}


$( "#tipo_doc" ).change(function() {
  //$rt= $(this).children("option:selected" ).val();
  $rt=$(this).val();
  if ($rt == 1) {
     //alert( "Handler for .change() called."+$rt );
      $("#numFact").css("display", "block");
  }else{
      if ($rt==2) {
           //alert( "Handler for .change() called."+$rt );
      $("#numFact").css("display", "block");
      }else{
    $("#numFact").css("display", "none");
  }

  }

 
});


  
</script>


@endsection