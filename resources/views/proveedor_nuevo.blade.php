
@extends('layouts.master')

@section('content')

 <section class="content">
    <div class="row">
    <div class="col-12">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
       <div class="card card-default">
            <div class="card-header">
               <h3 class="card-title">Nuevo Proveedor</h3>
            </div>
                <!-- /.card-header -->
             
         <!-- /.box-header -->
        <div class="card-body">
          
            <form class="form" method="POST" action="{{ route('nuevoProveedor') }}"  enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Nombre Proveedor</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa  fa-pencil-square-o"></i></span>
                      </div>
                      <input type="text" name="nombre_prov" class="form-control"  data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Direccion</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa fa-newspaper-o"></i></span>
                      </div>
                      <input type="text" name="prov_dir" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>E-mail</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa  fa-battery-1"></i></span>
                      </div>
                      <input type="text" name="prov_email" class="form-control" data-mask="">
                    </div>
                  </div>
                  
                  <!-- /.form-group --
                  <div class="form-group">
                    <label>Precio de Compra</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <i class="fa fa-dollar"></i>
                      </div>
                      <input type="text" name="precio_compra" class="form-control" data-mask="">
                    </div>
                  </div>
                  <-- /.form-group --
                   <div class="form-group">
                    <label>Unidad de Medida</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <i class="fa fa-balance-scale"></i>
                      </div>
                      <input type="text" name="unidad_med" class="form-control" data-mask="">
                    </div>
                  </div>
                  -- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Contacto</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa fa-barcode"></i></span>
                      </div>
                      <input type="text" name="prov_contacto" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Telefono</label>
                   <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa fa-bars"></i></span>
                      </div>
                     <input type="text" name="prov_telef" class="form-control" data-mask="">
                      
                    </div>
                  </div>
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Observacion</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                         <span class="input-group-text"><i class="fa fa-truck"></i></span>
                      </div>
                      <input type="text" name="prov_observ" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group --
                  <div class="form-group">
                    <label>Precio de Venta</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <i class="fa fa-dollar"> </i>
                      </div>
                      <input type="text" name="precio_venta" class="form-control" data-mask="">
                    </div>
                  </div>
                  <!-- /.form-group --
                   <!-- /.form-group --
                  <div class="form-group">
                    <label>Imagen de Producto</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <i class="fa fa-picture-o"></i>
                      </div>
                      <input type="file" name="imagen_prod" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                    </div>
                  </div>
                  -- /.form-group -->
                 
                </div>
                <div class="col-md-12" style="text-align: center;">
                <div class="form-group">
                    <div class="btn-group">
                  <button type="submit" class="btn btn-block btn-success btn-lg"> <i class="fa fa-save"></i> Guardar</button></div>
                  <div class="btn-group"><button type="button" class="btn btn-block btn-warning btn-lg" onclick="reset()"> <i class="fa fa-eraser"></i> Cancelar</button></div>
                  <div class="btn-group"><button type="button" class="btn btn-block btn-primary btn-lg"><i class="fa fa-undo"></i> Regresar</button></div>
                </div>
                <!-- /.col -->
              </div>
               </div>
          <!-- /.row -->
          </form>
         
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->

      <!-- /.row -->

    </section>


@endsection

@section('js')

 <script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script type="text/javascript">  

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    });
 
 
</script>


@endsection