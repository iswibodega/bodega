
{{-- \resources\views\roles\index.blade.php --}}


@extends('layouts.master')

@section('title', '| Roles')

@section('content')
 <section class="content">
    <div class="row">
    <div class="col-12">


     <div class="card">
     <div class="card-header">
    <h3><i class="fa fa-key"></i>Administrar Roles

    <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissions</a></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Role</th>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($roles as $role)
                <tr>

                    <td>{{ $role->name }}</td>

                    <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                    <td>
                    <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

<div class="pull-right">
      <a href="{{ URL::to('roles/create') }}" class="btn btn-success"><i class="fa fa-plus-square"></i> Agregar Rol</a>           
     </div>
     <hr>  

</div>
</div>
</div>
</section>

@endsection