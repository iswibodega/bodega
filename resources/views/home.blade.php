

@extends('layouts.master')

@section('content')


<section class="content-header">
      
    </section>
 <?php use \App\Http\Controllers\ProductoController; ?>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>Productos con mas Movimientos</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>Productos con mas Movimientos</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ ProductoController::prodductos_prestamo_cant()  }}</h3>

                <p>Productos en Prestamo</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{ url('/prestamo') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
               
                <h3> {{ ProductoController::inventario_minimo_cant()  }} </h3>

                <p>Productos con inventario minimo</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="{{ url('/inventario_min') }}" class="small-box-footer">Ver Detalle <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
         
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
     </div>
      
      <div class="container-fluid">   
        <div class="row">       
        
             
            <div class="col-lg-6">
            <div class="card">
              <div class="card-header">                
                  <h3 class="card-title">Cantida por Categoria</h3>
                  <div class="card-tools">
                 <button type="button" class="btn btn-tools" data-widget="collapse"><i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-tools" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
               <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Sales</th>
                    <th>More</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Some Product
                    </td>
                    <td>$13 USD</td>
                    <td>
                      <small class="text-success mr-1">
                        <i class="fa fa-arrow-up"></i>
                        12%
                      </small>
                      12,000 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Another Product
                    </td>
                    <td>$29 USD</td>
                    <td>
                      <small class="text-warning mr-1">
                        <i class="fa fa-arrow-down"></i>
                        0.5%
                      </small>
                      123,234 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Amazing Product
                    </td>
                    <td>$1,230 USD</td>
                    <td>
                      <small class="text-danger mr-1">
                        <i class="fa fa-arrow-down"></i>
                        3%
                      </small>
                      198 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Perfect Item
                      <span class="badge bg-danger">NEW</span>
                    </td>
                    <td>$199 USD</td>
                    <td>
                      <small class="text-success mr-1">
                        <i class="fa fa-arrow-up"></i>
                        63%
                      </small>
                      87 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  </tbody>
                </table>         
             </div>
            </div>
            <!-- /.card -->                       
          </div>


          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">                
                  <h3 class="card-title">Cantida por Categoria</h3>
                  <div class="card-tools">
                 <button type="button" class="btn btn-tools" data-widget="collapse"><i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-tools" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
               <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Sales</th>
                    <th>More</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Some Product
                    </td>
                    <td>$13 USD</td>
                    <td>
                      <small class="text-success mr-1">
                        <i class="fa fa-arrow-up"></i>
                        12%
                      </small>
                      12,000 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Another Product
                    </td>
                    <td>$29 USD</td>
                    <td>
                      <small class="text-warning mr-1">
                        <i class="fa fa-arrow-down"></i>
                        0.5%
                      </small>
                      123,234 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Amazing Product
                    </td>
                    <td>$1,230 USD</td>
                    <td>
                      <small class="text-danger mr-1">
                        <i class="fa fa-arrow-down"></i>
                        3%
                      </small>
                      198 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                      Perfect Item
                      <span class="badge bg-danger">NEW</span>
                    </td>
                    <td>$199 USD</td>
                    <td>
                      <small class="text-success mr-1">
                        <i class="fa fa-arrow-up"></i>
                        63%
                      </small>
                      87 Sold
                    </td>
                    <td>
                      <a href="#" class="text-muted">
                        <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                  </tbody>
                </table>
                            
             </div>
            </div>
            <!-- /.card -->            
          </div>

            
       </div>
      
   </div>

<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">

            <div class="card">
              <div class="card-header">                
                  <h3 class="card-title">Cantida por Area</h3>
                  <div class="card-tools">
                 <button type="button" class="btn btn-tools" data-widget="collapse"><i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-tools" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="box box-success">

            <div class="box-body">

            <div id="chartdiv" style="width: 100%; height: 400px;"></div>

            </div>

            <!-- /.box-body -->

          </div>
              </div>
            </div>
            <!-- /.card -->

            
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">                
                  <h3 class="card-title">Cantida por Categoria</h3>
                  <div class="card-tools">
                 <button type="button" class="btn btn-tools" data-widget="collapse"><i class="fa fa-minus"></i></button>
                 <button type="button" class="btn btn-tools" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                
               <div class="box box-success">            
                  <div class="box-body chart-responsive">

                   <div id="chartdiv2" style="width: 100%; height: 400px;"></div>

                  </div>
            <!-- /.box-body -->
              </div>                
             </div>
            </div>
            <!-- /.card -->

            
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

      

     </div>

    </section>

 

    <!-- /.content -->

    @endsection

 

 

@section('js')

<script src="{{ url('bower_components/raphael/raphael.min.js') }}"></script>

<script src="{{ url('bower_components/morris.js/morris.min.js') }}"></script>

 

<!-- DataTables -->


<script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

 

<script src="{{ url('bower_components/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>

<script src="{{ url('bower_components/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>

<script src="{{ url('bower_components/amcharts/amcharts/funnel.js') }}" type="text/javascript"></script>

  

<script type="text/javascript">

  var gp= <?php echo json_encode($grafprod); ?>;

 

  //var obj = JSON.parse(gp);

//alert(gp[0]['CANTIDAD']);

  //console.log(gp);
  var chart;

  var chartData=[];

   for (index = 0; index < gp.length; ++index) {

 

    chartData.push(gp[index]);

   // gp.push({color : '#4CAF50'});

 

}


//console.log(gp);

 

            AmCharts.ready(function () {

                // SERIAL CHART

                chart = new AmCharts.AmSerialChart();

                chart.dataProvider = chartData;

                chart.categoryField = "NOMBRE_TIPO_PRODUCTO";

                chart.startDuration = 1;

 

                // AXES

                // category

                var categoryAxis = chart.categoryAxis;

                categoryAxis.labelRotation = 45; // this line makes category values to be rotated

                categoryAxis.gridAlpha = 0;

                categoryAxis.fillAlpha = 1;

                categoryAxis.fillColor = "#FAFAFA";

                categoryAxis.gridPosition = "start";

 

                // value

                var valueAxis = new AmCharts.ValueAxis();

                valueAxis.dashLength = 5;

                valueAxis.title = "";

                valueAxis.axisAlpha = 0;

                chart.addValueAxis(valueAxis);

 

                // GRAPH

                var graph = new AmCharts.AmGraph();

                graph.valueField = "CANTIDAD";

                graph.colorField = "COLOR";

                graph.balloonText = "<b>[[category]]: [[value]]</b>";

                graph.type = "column";

                graph.lineAlpha = 0;

                graph.fillAlphas = 1;

                chart.addGraph(graph);

 

                // CURSOR

                var chartCursor = new AmCharts.ChartCursor();

                chartCursor.cursorAlpha = 0;

                chartCursor.zoomable = false;

                chartCursor.categoryBalloonEnabled = false;

                chart.addChartCursor(chartCursor);

 

                //chart.creditsPosition = "bottom-right";

 

                // WRITE

                chart.write("chartdiv2");

            });

 

</script>

 

 

<script>

   var gpp= <?php echo json_encode($grafphi); ?>;

  //var obj = JSON.parse(gp);

//alert(gp[0]['CANTIDAD']);

  //console.log(gp);

    
  var chart;

  var data=[];

   for (index = 0; index < gpp.length; ++index) {

 

    data.push(gpp[index]);

   // gp.push({color : '#4CAF50'});

}

 
            AmCharts.ready(function () {

 

                chart = new AmCharts.AmFunnelChart();

                chart.rotate = true;

                chart.titleField = "TIPO_PRODUCTO";

                chart.balloon.fixedPosition = true;

                chart.marginRight = 210;

                chart.marginLeft = 15;

                chart.labelPosition = "right";

                chart.funnelAlpha = 0.9;

                chart.valueField = "CANTIDAD";

                chart.startX = -500;

                chart.dataProvider = data;

                chart.startAlpha = 0;

                chart.depth3D = 100;

                chart.angle = 30;

                chart.outlineAlpha = 1;

                chart.outlineThickness = 2;

                chart.outlineColor = "#FFFFFF";

                chart.write("chartdiv");

            });

        </script>

    <script type="text/javascript">

 
 

function Ocultar() {

     $( "#tablaS" ).css('display','none');

      $( "#btnOcl" ).css('display','none');

}

 

 

</script>

 
